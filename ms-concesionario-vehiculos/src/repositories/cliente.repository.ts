import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Cliente, ClienteRelations, Vehiculo} from '../models';
import {VehiculoRepository} from './vehiculo.repository';

export class ClienteRepository extends DefaultCrudRepository<
  Cliente,
  typeof Cliente.prototype._id,
  ClienteRelations
> {

  public readonly id_vehiculo: BelongsToAccessor<Vehiculo, typeof Cliente.prototype._id>;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource, @repository.getter('VehiculoRepository') protected vehiculoRepositoryGetter: Getter<VehiculoRepository>,
  ) {
    super(Cliente, dataSource);
    this.id_vehiculo = this.createBelongsToAccessorFor('id_vehiculo', vehiculoRepositoryGetter,);
    this.registerInclusionResolver('id_vehiculo', this.id_vehiculo.inclusionResolver);
  }
}
