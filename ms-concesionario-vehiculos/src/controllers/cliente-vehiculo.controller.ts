import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Cliente,
  Vehiculo,
} from '../models';
import {ClienteRepository} from '../repositories';

export class ClienteVehiculoController {
  constructor(
    @repository(ClienteRepository)
    public clienteRepository: ClienteRepository,
  ) { }

  @get('/clientes/{id}/vehiculo', {
    responses: {
      '200': {
        description: 'Vehiculo belonging to Cliente',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Vehiculo)},
          },
        },
      },
    },
  })
  async getVehiculo(
    @param.path.string('id') id: typeof Cliente.prototype._id,
  ): Promise<Vehiculo> {
    return this.clienteRepository.id_vehiculo(id);
  }
}
