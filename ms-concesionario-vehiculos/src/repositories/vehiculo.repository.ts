import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Vehiculo, VehiculoRelations, Cliente} from '../models';
import {ClienteRepository} from './cliente.repository';

export class VehiculoRepository extends DefaultCrudRepository<
  Vehiculo,
  typeof Vehiculo.prototype._id,
  VehiculoRelations
> {

  public readonly clienteVehiculo: HasManyRepositoryFactory<Cliente, typeof Vehiculo.prototype._id>;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource, @repository.getter('ClienteRepository') protected clienteRepositoryGetter: Getter<ClienteRepository>,
  ) {
    super(Vehiculo, dataSource);
    this.clienteVehiculo = this.createHasManyRepositoryFactoryFor('clienteVehiculo', clienteRepositoryGetter,);
  }
}
